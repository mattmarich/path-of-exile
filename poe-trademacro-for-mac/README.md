## Run PoE TradeMacro on Mac<br/><br/>  
**Step 1: Install Window Emulator Software**<br/>
(Note: I am not affiliated with any of the following mentioned companies)<br/><br/>
**MacOS <= 10.14**<br/>
Install WineHQ which is free: https://dl.winehq.org/wine-builds/macosx/download.html<br/><br/>
**MacOS >= 10.15**<br/>
At time of writting, WineHQ does not have a stable version. I personally have had better luck with CrossOver from Codeweavers so the purchase price doesn't bother me. The purchase price however is $40 USD for current version (NOTE THIS: there are typically breaking changes between MacOS Major releases ie from 10.13 to 10.14 and 10.14 to 10.15) which has a 14-day trial and can be downloaded from: https://www.codeweavers.com/crossover/download-now<br/><br/>
When you install either WineHQ or CrossOver, you will be creating your Windows 'Bottle', this is a container where all associated software is located for running your TradeMacro (and anything else youd like to add following)<br/>
Create your Windows 10 Bottle (For now I'll defer you to the Wine/CrossOver documentation if the Walkthrough Wizard doesn't get you up and running, in the future I may add those steps)<br/><br/>
**Step 2: Install Windows .NET Framework to your bottle**<br/>
There are many Windows Packages available for installation in the mentioned emulators, search for 'Microsoft .NET Framework 4.0' and install this package.<br/><br/>
**Step 3: Download PoE-TradeMacro**<br/>
Download the PoE-TradeMacro zip from: https://github.com/PoE-TradeMacro/POE-TradeMacro/archive/master.zip<br/>
Unzip<br/>
Move Unzipped folder into your Windows Bottle<br/><br/>
**Step 4: Rinse and Repeat for any other tools you would like to use**<br/>
Do note that other applications may need additional packages, many standard packages are available for install through the emulator. For any requirements which are not included, you may download the Windows executable and follow the Emulators 'Install a Windows Application' Wizard<br/><br/>
**Step 5: RUN!**<br/>
Lets get that PoE-TradeMacro launched and get back to running some content!
